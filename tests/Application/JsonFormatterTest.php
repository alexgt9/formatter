<?php

namespace Magento\Application;

use Magento\Domain\Report;
use PHPUnit\Framework\TestCase;

final class JsonFormatterTest extends TestCase
{
    public function testThatReportIsFormatted()
    {
        $date = new \DateTime();
        $report = Report::create('Title', 'Content', $date);

        $expected = <<<Json
{"title":"Title","date":"{$date->format('y-m-d h:i:s')}","content":"Content"}
Json;

        $formatter = new JsonFormatter();

        $this->assertEquals($expected, $formatter->format($report));
    }
}
