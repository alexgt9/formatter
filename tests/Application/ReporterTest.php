<?php

namespace Magento\Tests;

use Magento\Application\Reporter;
use Magento\Domain\FormatterFactoryInterface;
use Magento\Domain\FormatterInterface;
use Magento\Domain\MailerInterface;
use Magento\Domain\Report;
use Magento\Domain\ReportFormat;
use PHPUnit\Framework\TestCase;

class ReportTest extends TestCase
{
    private $mockedFormatter;
    private $mockedFormatterFactory;
    private $mockedMailer;

    public function setUp()
    {
        $this->mockedFormatter = $this->createMock(FormatterInterface::class);
        $this->mockedFormatterFactory = $this->createMock(FormatterFactoryInterface::class);
        $this->mockedMailer = $this->createMock(MailerInterface::class);

        $this->reporter = new Reporter($this->mockedFormatterFactory, $this->mockedMailer);
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Title can't be empty
     */
    public function testThatNonValidTitleThrowsException()
    {
        Report::create('', 'Content');
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Body can't be empty
     */
    public function testThatNonValidBodyThrowsException()
    {
        Report::create('Title', '');
    }

    public function testThatReportIsSendInJsonFormat()
    {
        $date = new \DateTime();
        $report = Report::create('Title', 'Body', $date);
        $format = ReportFormat::JSON();

        $formatted = 'Wherever';

        $this->shouldGetTheFormatFormatter($format);
        $this->reportShouldBeFormatted($report, $formatted);
        $this->mailShouldBeSent($formatted);

        $this->reporter->sendReport($report, $format);
    }

    private function shouldGetTheFormatFormatter(ReportFormat $format)
    {
        $this->mockedFormatterFactory
            ->method('create')
            ->with($format)
            ->willReturn($this->mockedFormatter);
    }

    private function reportShouldBeFormatted(Report $report, string $content)
    {
        $this->mockedFormatter
            ->expects($this->once())
            ->method('format')
            ->with($report)
            ->willReturn($content);
    }

    private function mailShouldBeSent(string $content)
    {
        $this->mockedMailer
            ->expects($this->once())
            ->method('send')
            ->with($content);
    }
}
