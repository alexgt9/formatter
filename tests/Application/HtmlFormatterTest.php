<?php

namespace Magento\Application;

use Magento\Domain\Report;
use PHPUnit\Framework\TestCase;

final class HtmlFormatterTest extends TestCase
{
    public function testThatReportIsFormatted()
    {
        $date = new \DateTime();
        $report = Report::create('Title', 'Content', $date);

        $expected = <<<HTML
<h1>Title</h1>
<p>{$date->format('y-m-d h:i:s')}</p>
<div class='content'>Content</div>
HTML;

        $formatter = new HtmlFormatter();

        $this->assertEquals($expected, $formatter->format($report));
    }
}
