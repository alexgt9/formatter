<?php

namespace Magento\Domain;

interface FormatterFactoryInterface
{
    public function create(ReportFormat $format): FormatterInterface;
}
