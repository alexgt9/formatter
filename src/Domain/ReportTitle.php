<?php

namespace Magento\Domain;

final class ReportTitle
{
    private $value;

    public function __construct(string $title)
    {
        if (empty($title)) {
            throw new \InvalidArgumentException("Title can't be empty");
        }

        $this->value = $title;
    }

    public function value()
    {
        return $this->value;
    }
}
