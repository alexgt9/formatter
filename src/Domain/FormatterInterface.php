<?php

namespace Magento\Domain;

interface FormatterInterface
{
    public function format(Report $report): string;
}
