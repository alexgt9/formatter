<?php

namespace Magento\Domain;

interface MailerInterface
{
    public function send(string $body);
}
