<?php

namespace Magento\Domain;

final class ReportDate
{
    private $value;

    public function __construct(\DateTime $date)
    {
        $this->value = $date;
    }

    public function value(): \DateTime
    {
        return $this->value;
    }
}
