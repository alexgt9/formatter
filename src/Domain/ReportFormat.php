<?php

namespace Magento\Domain;

use MyCLabs\Enum\Enum;

final class ReportFormat extends Enum
{
    const JSON = 'json';
    const HTML = 'html';
}
