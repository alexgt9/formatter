<?php

namespace Magento\Domain;

final class ReportBody
{
    private $value;

    public function __construct(string $body)
    {
        if (empty($body)) {
            throw new \InvalidArgumentException("Body can't be empty");
        }

        $this->value = $body;
    }

    public function value()
    {
        return $this->value;
    }
}
