<?php

namespace Magento\Domain;

final class Report
{
    private $title;
    private $date;
    private $body;

    public function __construct(ReportTitle $title, ReportBody $body, ReportDate $date = null)
    {
        $this->title = $title;
        $this->date = $date;
        $this->body = $body;
    }

    public static function create(string $title, string $body, \DateTime $date = null)
    {
        $date = $date ?? new \DateTime();

        return new self(
            new ReportTitle($title),
            new ReportBody($body),
            new ReportDate($date)
        );
    }

    public function title()
    {
        return $this->title;
    }

    public function body()
    {
        return $this->body;
    }

    public function date()
    {
        return $this->date;
    }

    public function formatJson()
    {
        return json_encode($this->reportToArray());
    }

    public function reportToArray()
    {
        return [
            'title' => $this->title,
            'date' => $this->date,
            'content' => $this->content
        ];
    }

    public function formatHtml()
    {
        return "
            <h1>{$this->title}</h1> .
            <p>{$this->date}</p> .
            <div class='content'>{$this->content}</div> .
        ";
    }

    public function sendReport($type)
    {
        if ($this->validate()) {
            if ($type == 'HTML') {
                $mailer = new Mailer();
                $mailer->send($this->formatHtml());
                return true;
            }

            if ($type == 'JSON') {
                $mailer = new Mailer();
                $mailer->send($this->formatJson());
                return true;
            }
        }

        return false;
    }
}
