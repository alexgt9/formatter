<?php

namespace Magento\Infrastructure;

final class Mailer implements MailerInterface
{
    public function send(string $body)
    {
        $this->sendMailWithARealMailer($body);
        
        return true;
    }
}
