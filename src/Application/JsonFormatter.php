<?php

namespace Magento\Application;

use Magento\Domain\FormatterInterface;
use Magento\Domain\Report;

final class JsonFormatter implements FormatterInterface
{
    public function format(Report $report): string
    {
        return json_encode($this->reportToArray($report));
    }

    public function reportToArray(Report $report)
    {
        return [
            'title' => $report->title()->value(),
            'date' => $report->date()->value()->format('y-m-d h:i:s'),
            'content' => $report->body()->value()
        ];
    }
}
