<?php

namespace Magento\Application;

use Magento\Domain\FormatterFactoryInterface;
use Magento\Domain\MailerInterface;
use Magento\Domain\Report;
use Magento\Domain\ReportFormat;

final class Reporter
{
    private $formatterFactory;
    private $mailer;

    public function __construct(FormatterFactoryInterface $formatterFactory, MailerInterface $mailer)
    {
        $this->formatterFactory = $formatterFactory;
        $this->mailer = $mailer;
    }

    public function sendReport(Report $report, ReportFormat $format)
    {
        $formatter = $this->formatterFactory->create($format);
        $this->mailer->send($formatter->format($report));

        return true;
    }
}
