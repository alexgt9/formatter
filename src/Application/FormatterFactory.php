<?php

namespace Magento\Application;

class FormatterFactory implements FormatterFactoryInterface
{
    public function __construct(array $formatters)
    {
        $this->formatters = $formatters;
    }
    public function create(ReportFormat $format)
    {
        if (isset($this->formatters[$format->value()])) {
            return $this->formatters[$format->value()];
        }

        throw new RuntimeException("Factory {$format->value()} not found");
    }
}
