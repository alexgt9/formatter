<?php

namespace Magento\Application;

use Magento\Domain\FormatterInterface;
use Magento\Domain\Report;

final class HtmlFormatter implements FormatterInterface
{
    public function format(Report $report): string
    {
        return <<<HTML
<h1>{$report->title()->value()}</h1>
<p>{$report->date()->value()->format('y-m-d h:i:s')}</p>
<div class='content'>{$report->body()->value()}</div>
HTML;
    }
}
